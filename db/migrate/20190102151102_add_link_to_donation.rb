class AddLinkToDonation < ActiveRecord::Migration[5.2]
  def change
    add_column :donations, :link, :string
  end
end
