class CreateDonations < ActiveRecord::Migration[5.2]
  def change
    create_table :donations do |t|
      t.string :address
      t.integer :status
      t.string :seed
      t.string :bundleHash
      t.string :name
      t.string :message

      t.timestamps
    end
  end
end
