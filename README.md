# Donation Manager

[![pipeline status](https://gitlab.com/einfachiota/donation-manager/badges/master/pipeline.svg)](https://gitlab.com/einfachiota/donation-manager/commits/master)

The donation manager is a helper to manage IOTA Donations.

* Create permanent Donations with name, message, url and in image.
* Distribute donations to multiple addresses.
* Add Donations to any website through a popup which handles the donation process.
