FactoryBot.define do
  factory :donation do
    address { "MyString" }
    status { 1 }
    seed { "MyString" }
    bundleHash { "MyString" }
  end
end
