FactoryBot.define do
  factory :notification do
    recipient_id { "" }
    actor_id { "" }
    read_at { "2019-01-01 00:08:54" }
    action { "MyString" }
    notifiable_id { "" }
    notifiable_type { "MyString" }
  end
end
