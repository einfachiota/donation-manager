FactoryBot.define do
  factory :announcement do
    published_at { "2019-01-01 00:08:53" }
    announcement_type { "MyString" }
    name { "MyString" }
    description { "MyText" }
  end
end
