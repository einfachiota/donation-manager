require 'rails_helper'

RSpec.describe "donations/new", type: :view do
  before(:each) do
    assign(:donation, Donation.new(
      :address => "MyString",
      :status => 1,
      :seed => "MyString",
      :bundleHash => "MyString"
    ))
  end

  it "renders new donation form" do
    render

    assert_select "form[action=?][method=?]", donations_path, "post" do

      assert_select "input[name=?]", "donation[address]"

      assert_select "input[name=?]", "donation[status]"

      assert_select "input[name=?]", "donation[seed]"

      assert_select "input[name=?]", "donation[bundleHash]"
    end
  end
end
