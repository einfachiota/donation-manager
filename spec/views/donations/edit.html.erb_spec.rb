require 'rails_helper'

RSpec.describe "donations/edit", type: :view do
  before(:each) do
    @donation = assign(:donation, Donation.create!(
      :address => "MyString",
      :status => 1,
      :seed => "MyString",
      :bundleHash => "MyString"
    ))
  end

  it "renders the edit donation form" do
    render

    assert_select "form[action=?][method=?]", donation_path(@donation), "post" do

      assert_select "input[name=?]", "donation[address]"

      assert_select "input[name=?]", "donation[status]"

      assert_select "input[name=?]", "donation[seed]"

      assert_select "input[name=?]", "donation[bundleHash]"
    end
  end
end
