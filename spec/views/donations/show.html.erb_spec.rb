require 'rails_helper'

RSpec.describe "donations/show", type: :view do
  before(:each) do
    @donation = assign(:donation, Donation.create!(
      :address => "Address",
      :status => 2,
      :seed => "Seed",
      :bundleHash => "Bundle Hash"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Seed/)
    expect(rendered).to match(/Bundle Hash/)
  end
end
