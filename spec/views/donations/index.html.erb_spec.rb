require 'rails_helper'

RSpec.describe "donations/index", type: :view do
  before(:each) do
    assign(:donations, [
      Donation.create!(
        :address => "Address",
        :status => 2,
        :seed => "Seed",
        :bundleHash => "Bundle Hash"
      ),
      Donation.create!(
        :address => "Address",
        :status => 2,
        :seed => "Seed",
        :bundleHash => "Bundle Hash"
      )
    ])
  end

  it "renders a list of donations" do
    render
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Seed".to_s, :count => 2
    assert_select "tr>td", :text => "Bundle Hash".to_s, :count => 2
  end
end
