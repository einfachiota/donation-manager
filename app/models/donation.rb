class Donation < ApplicationRecord
  require 'iota'
  serialize :distributon_data, JSON

  before_create :create_address, :fetch_distributions

  enum status: [:waiting, :payout_in_progress, :payout_proccessed]

  def sync!
    return unless self.waiting?
    # Create client directly with provider
    client = IOTA::Client.new(provider: 'https://nodes.devnet.thetangle.org:443')
    account = IOTA::Models::Account.new(client, self.seed)
    account.getAccountDetails({})
    pp "balance"
    pp account.balance

    if account.balance > 0
      pp "donated!"
      self.payout_in_progress!
      DonationDistributionJob.perform_later(self)
    else
      pp "not donated!"
    end

  end

  private
  def create_address
    # Create client directly with provider
    client = IOTA::Client.new(provider: 'https://nodes.devnet.thetangle.org:443')

    # now you can start using all of the functions
    status, data = client.api.getNodeInfo

    ## create and save seed
    self.seed = (1..81).map{'ABCDEFGHIJKLMNOPQRSTUVWXYZ9'[SecureRandom.random_number(27)]}.join('')

    ## create and save latest address
    account = IOTA::Models::Account.new(client, self.seed)
    account.getAccountDetails({})

    self.address = account.latestAddress
    self.status = :waiting
  end

  def fetch_distributions
    uri = URI("#{Rails.configuration.pool_manager_url}/get_node_payout_data.json")
    response =  Net::HTTP.get(uri)# => String
    self.distributon_data = JSON.parse(response)

  end

end
