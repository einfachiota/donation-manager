class HomeController < ApplicationController
  def index
    @donations = Donation.where(status: :payout_proccessed)
  end

  def terms
  end

  def privacy
  end

  def donations
  end
end
