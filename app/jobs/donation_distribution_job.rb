class DonationDistributionJob < ApplicationJob
  queue_as :default
  require 'iota'
  require 'json'

  def perform(donation)
    pp "DonationDistributionJob"
    client = IOTA::Client.new(provider: 'https://nodes.devnet.thetangle.org:443')

    account = IOTA::Models::Account.new(client, donation.seed)
    account.getAccountDetails({})
    balance  = account.balance.to_i

    uri = URI("#{Rails.configuration.pool_manager_url}/get_node_payout_data.json")
    distribution_response =  Net::HTTP.get(uri)# => String
    pp "distribution_response"
    pp distribution_response

    distributon_data = JSON.parse(distribution_response)

    pp "distributon_data"
    pp distributon_data

    pp "classes"
    pp distribution_response.class
    pp donation.distributon_data.class
    transfers = []
    sum = 0
    distributon_data["entries"].each do |distribution|
      value = (balance * distribution["share"]).to_i
      address = distribution["address"]

      pp "value"
      pp value
      pp value.class

      pp "address"
      pp address
      sum = sum + value

      transfer = IOTA::Models::Transfer.new(
          address: address,
          value: value,
          tag: 'DONATION9MANAGER'
      )
      pp "transfer"
      pp transfer
      transfers.push(transfer)

    end

    remainder = balance - sum

    if remainder > 0
      pp "remainder exists"
      pp remainder
      transfer = IOTA::Models::Transfer.new(
          address: "OMCMMOFFGBECDKYERTOYTAVUAGUVDAJCYN9JR9IZGGCTQSUKVHJQOJXALJPD9OFDPUFVPGC9PPFPGZZIWEMSNHIRUX",
          value: remainder,
          tag: 'REMAINDER999DONATIONSERVICE'
      )
      transfers.push(transfer)
    end



    pp "transfers"
    pp transfers

    depth = 3
    minWeightMagnitude = 14

    # If you want library to fetch input and make transfer
    # TODO: Check response
    response = account.sendTransfer(depth, minWeightMagnitude, transfers, {})
    pp "response"
    pp response
    pp "response.first"
    pp response.first
    pp "response.first.bundle"
    pp response.first.bundle
    donation.distributon_data = distributon_data
    donation.bundleHash = response.first.bundle
    donation.payout_proccessed!
  end
end
