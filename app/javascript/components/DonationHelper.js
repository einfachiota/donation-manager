import React from "react"
import PropTypes from "prop-types"

import { composeAPI } from '@iota/core'
import * as IOTAQR from "@tangle-frost/iota-qr-lib/pkg/iota-qr-lib"

const iota = composeAPI({
  provider: 'https://nodes.devnet.thetangle.org:443'
})


class DonationHelper extends React.Component {

  constructor(props) {
    super(props);
    // Don't call this.setState() here!
    this.state = {
      balance: 0,
      qrElement: '',
    };
    this.getBalance(props.address)
    this.renderQR()
  }

  async componentDidMount() {

  }

  async renderQR() {
    var qrElement = await IOTAQR.AddressQR.renderHtml(this.props.address, "png");
    this.setState({ qrElement: qrElement });
  }


  getBalance(address) {
    // https://github.com/iotaledger/iota.js/blob/next/api_reference.md#coregetbalancesaddresses-threshold-callback
    var self = this

    iota.getBalances([address], 100)
      .then(({ balances }) => {
        balances.forEach((balance) => {
          this.setState({
              balance: this.state.balance + balance
          })
        })
      })
      .catch(err => {})
    }

  copyAddress(){
    console.log("Add copy user feedback");
  }

  render () {
    return (
      <React.Fragment>
        <div className="card text-center">
          <div className="card-body">
            <h4 className="card-title"><strong>{this.state.balance}</strong> IOTA</h4>
            <h6 className="card-title">Current balance</h6>
            <p className="card-text">Send your donation to this <strong>IOTA devnet address:</strong></p>
            <img src={this.state.qrElement.src}/>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

DonationHelper.propTypes = {
  address: PropTypes.string
};
export default DonationHelper
